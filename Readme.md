You can find the deployed web app at http://34.71.252.55:9000/

visit http://34.71.252.55:9000/submit  to create a new profile

visit http://34.71.252.55:9000/login   to check if user exists or not to login

visit http://34.71.252.55:9000/  to list all profiles


### first clone the repo:
- git clone https://gitlab.com/MahammadAGHK/devops-lab3
- cd devops-lab3



You can look at my Dockerfile which is in repo.

### To go to ansible folder and see playbooks and roles
- cd ansible

You can also find all ansible related things in ansible/ folder in the repo.

I have scpeified my hosts and some variables in inventory file.


I have playbook-docker.yml which installs docker on server

### To run first docker install playbook:
- ansible-playbook -i inventory playbook-docker.yml --ask-vault-pass

there are three roles which I have created: app, database, test


First role creates mysql database from mysql image pulled from dockerhub
 
I have used ansible-vault to encrypt mysql database credentials and database name

ansible-vault encrypt database/vars/vault.yml

the variables file is in database/vars/vault.yml and my main tasks are in database/tasks/main.yml

Then playbook-db.yml uses databse role and installs mysql using docker on server

### To run database initialization playbook:
- ansible-playbook -i inventory playbook-db.yml --ask-vault-pass

First we run playbook-build.yml which builds docker image of app from Dockerfile and the pushes image to my dockerhub container registry

All credentials and variables are in vars.yml which has been encrypted using ansible-vault

### To run image building playbook:
- ansible-playbook -i inventory playbook-build.yml --ask-vault-pass

The second role which is app role creates the application 

Then my app role pulles docker image from dockerhub contaienr registry and then creates container and run the app 

again all variables are in app/vars/vault.yml

playbook-run.yml uses app role and runs the app 

### To run app creation and deploying container playbook:
- ansible-playbook -i inventory playbook-run.yml --ask-vault-pass

The final role is test role which just pulls image from container registry and runs image but this time with command npm run tests 

which are executed and then we look at docker logs login-register-app-test and see the results 


all variables related to test are in test/vars/vault.yml 

I have created playbook-app.yml which uses all the roles database, test and app and combines them at once

and all variable are inside roles in vault.yml files encrypted with ansible vault

### To run all in once database, tests and app deployment playbook:
- ansible-playbook -i inventory playbook-app.yml --ask-vault-pass

also apart from test role I have also written test.py script which is in repo that performs same tests and checks the health of application and database you can just simply run that script using this command:

### To run tests using python to Verify the web application is accessible and functioning properly and Ensure the database connection is established and data can be read/written

- python3 test.py

## I have put ansible vault secret password in the file in LMS you can take it from there


## I have also created a Gitlab CI/CD using .gitlab_ci.yml file which has three stages: build, test and deploy I have put it as a template which is actually not applied









