import requests

base_url = 'http://34.71.252.55:9000'  # Update the base URL based on your app's configuration

def test_get_request():
    response = requests.get(base_url)
    assert response.status_code == 200
    assert 'Reading data from database' in response.text
    print('Test Get Request: Success - Received expected response.')

def test_successful_user_registration():
    data = {
        'name': 'John',
        'surname': 'Doe',
        'age': 30,
        'username': 'johndoe',
        'password': 'Password1!'
    }
    response = requests.post(f'{base_url}/submit', data=data)
    assert response.status_code == 200
    assert 'Record successfullly submitted to database!' in response.text
    print('Test Successful User Registration: Success - Received expected response.')

def test_invalid_user_registration_no_capital_letter():
    data = {
        'name': 'Mark',
        'surname': 'Taylor',
        'age': 30,
        'username': 'marktaylor',
        'password': 'password1'
    }
    response = requests.post(f'{base_url}/submit', data=data)
    assert response.status_code == 200
    assert 'password should contain at least one capital letter' in response.text
    print('Test Invalid User Registration (No Capital Letter): Success - Received expected response.')

def test_invalid_user_registration_no_number():
    data = {
        'name': 'Mark',
        'surname': 'Taylor',
        'age': 30,
        'username': 'marktaylor',
        'password': 'Passwordddd'
    }
    response = requests.post(f'{base_url}/submit', data=data)
    assert response.status_code == 200
    assert 'password should contain at least one number' in response.text
    print('Test Invalid User Registration (No Number): Success - Received expected response.')

def test_successful_user_login():
    data = {
        'username': 'johndoe',
        'password': 'Password1!'
    }
    response = requests.post(f'{base_url}/login', data=data)
    assert response.status_code == 200
    assert 'You successfully logined' in response.text
    print('Test Successful User Login: Success - Received expected response.')

def test_invalid_user_login_wrong_username():
    data = {
        'username': 'markangelo',
        'password': 'Password1!'
    }
    response = requests.post(f'{base_url}/login', data=data)
    assert response.status_code == 200
    assert 'username or password incorrect' in response.text
    print('Test Invalid User Login (Wrong Username): Success - Received expected response.')

def test_invalid_user_login_wrong_password():
    data = {
        'username': 'johndoe',
        'password': 'Password123'
    }
    response = requests.post(f'{base_url}/login', data=data)
    assert response.status_code == 200
    assert 'username or password incorrect' in response.text
    print('Test Invalid User Login (Wrong Password): Success - Received expected response.')

def run_tests():
    try:
        test_get_request()
        test_successful_user_registration()
        test_invalid_user_registration_no_capital_letter()
        test_invalid_user_registration_no_number()
        test_successful_user_login()
        test_invalid_user_login_wrong_username()
        test_invalid_user_login_wrong_password()
        print('All tests passed!')
    except AssertionError as e:
        print(f'Test failed: {e}')

if __name__ == '__main__':
    run_tests()


