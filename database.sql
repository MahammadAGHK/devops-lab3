use appdb;
create table users(
    username varchar(30) primary key,
    name varchar(30) not null,
    surname varchar(30) not null,
    age int(2) not null,
    password varchar(255) not null
);
