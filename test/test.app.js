const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../app.js'); // Update the path to your app.js
const expect = chai.expect;

chai.use(chaiHttp);

describe('App Tests', () => {
  it('should return a list of users on GET /', (done) => {
    chai.request(app)
      .get('/')
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.text).to.include('Reading data from database');
        done();
      });
  });

  it('should return a "Success" message on successful user registration', (done) => {
    chai.request(app)
      .post('/submit')
      .send('name=John&surname=Doe&age=30&username=johndoe&password=Password1!')
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.text).to.include('Record successfullly submitted to database!');
        done();
      });
  });

  it('should return an error message on invalid user registration because of no capital letter in password', (done) => {
    chai.request(app)
      .post('/submit')
      .send('name=Mark&surname=Taylor&age=30&username=marktaylor&password=password1')
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.text).to.include('password should contain at least one capital letter');
        done();
      });
  });
  
  it('should return an error message on invalid user registration because of no number in password', (done) => {
    chai.request(app)
      .post('/submit')
      .send('name=Mark&surname=Taylor&age=30&username=marktaylor&password=Passwordddd')
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.text).to.include('password should contain at least one number');
        done();
      });
  });
  it('should return a "Success" message on successful user login', (done) => {
    chai.request(app)
      .post('/login')
      .send('username=johndoe&password=Password1!')
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.text).to.include('You successfully logined');
        done();
      });
  });

  it('should return an error message on invalid user login because of wrong username', (done) => {
    chai.request(app)
      .post('/login')
      .send('username=markangelo&password=Password1!')
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.text).to.include('username or password incorrect');
        done();
      });
  });
  it('should return an error message on invalid user login because of wrong password', (done) => {
    chai.request(app)
      .post('/login')
      .send('username=johndoe&password=Password123')
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.text).to.include('username or password incorrect');
        done();
      });
  });
});

process.on('exit', function(code) {
  if (code === 0) {
    console.log('Tests passed!');
  } else {
    console.log('Tests failed!');
    process.exit(1);
  }
});